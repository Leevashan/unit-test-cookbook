export class Recipe {
    title: string;
    description: string;
    codeExample: Page[];
}

export class Page {
    pageType: string;
    description: string;
    code: string;
}
