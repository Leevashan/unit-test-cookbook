import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppComponent} from './app.component';
import {UnitTestRecipeComponent} from './unit-test-recipe/unit-test-recipe.component';
import {UnitTestRecipeModule} from './unit-test-recipe/unit-test-recipe.module';

const routes: Routes = [
  {
    path: 'recipe',
    component: UnitTestRecipeComponent
  },
  {
    path: '',
    redirectTo: '/recipe',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    UnitTestRecipeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
