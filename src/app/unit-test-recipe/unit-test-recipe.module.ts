import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UnitTestRecipeComponent} from './unit-test-recipe.component';
import {MatCardModule, MatExpansionModule, MatSidenavModule, MatToolbarModule} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatToolbarModule,
    MatSidenavModule,
    MatExpansionModule,
  ],
  declarations: [UnitTestRecipeComponent],
  exports: [UnitTestRecipeComponent],
})
export class UnitTestRecipeModule {
}
