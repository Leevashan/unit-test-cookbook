import {Component, OnInit} from '@angular/core';
import {Recipe} from '../recipe';

@Component({
  selector: 'app-unit-test-recipe',
  templateUrl: './unit-test-recipe.component.html',
  styleUrls: ['./unit-test-recipe.component.css'],
})
export class UnitTestRecipeComponent implements OnInit {

  public recipes: Recipe[];
  public selected: Recipe = null;
  public loading = false;

  constructor() {
  }

  ngOnInit() {
    this.setTest();
  }

  setTest() {

    this.recipes = [
      {
        title: 'Check if selector exists',
        description: `
      This test shows how to test if a control has been injected into another component when the html is generated.
      <br>
      In this test we will cover adding a route-outlet tag to the main components html template, to accomplish this test
      we first add the routing outlet tag to the components template, then we need to import the 'RouterTestingModule'
      module from '@angular/router/testing' into our spec.ts file.

      `,
        codeExample: [{
          pageType: 'component.specs.ts',
          description: 'This test just creates the component and then checks the components html for a tag',
          code: `
        import { async, ComponentFixture, TestBed } from '@angular/core/testing';
        import { RouterTestingModule } from '@angular/router/testing';

        import { AppComponent } from './app.component';

        describe('AppComponent', () => {

          beforeEach(async(() => {
            TestBed.configureTestingModule({
              declarations: [
                AppComponent
              ],
              imports: [
                RouterTestingModule
              ]
            }).compileComponents();
          }));

          it('should render router-outlet tag', async(() => {
            fixture = TestBed.createComponent(AppComponent);
            fixture.detectChanges();
            const compiled = fixture.debugElement.nativeElement;
            expect(compiled.querySelector('router-outlet')).not.toBe(null);
            expect(compiled.querySelector('router-outlet')).toBeTruthy();
          }));

        });`
        },
          {
            pageType: 'component.ts',
            description: 'Basic app.component.ts',
            code: `
        import { Component } from '@angular/core';
        @Component({
          selector: 'app-root',
          template: '<router-outlet></router-outlet>',
          styleUrls: []
        })
        export class AppComponent {}
        `
          }]
      }, {
        title: 'Testing an API call',
        description: `
      This test will show you how to test a call to an api using HttpClient.
      <br>
      In this test we will cover adding a route-outlet tag to the main components html template, to accomplish this test
      we first add the routing outlet tag to the components template, then we need to import the 'RouterTestingModule'
      module from '@angular/router/testing' into our spec.ts file.

      `,
        codeExample: [{
          pageType: 'minutes.service.specs.ts',
          description: `Your spec needs to import both http modules from '@angular/common/http/testing'
                      You want to ensure that none of the api calls you have actually go out to a real api, \
                      you need to mock these calls and return your mock data`,
          code: `
        import { TestBed, inject, async } from '@angular/core/testing';
        import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

        import { MinutesService } from './minutes.service';

        const apiUrl = 'https://api.qa.allaboutminutes.com/api/';

        describe('MinutesService', () => {
          beforeEach(() => {
            TestBed.configureTestingModule({
              imports: [ HttpClientTestingModule ],
              providers: [MinutesService]
            });
          });

          it('should be created', inject([MinutesService], (service: MinutesService) => {
            expect(service).toBeTruthy();
          }));

          it('should have a function called getDashboardMinutes', inject([MinutesService], (service: MinutesService) => {
            expect(service.getDashboardMinutes).toBeDefined();
          }));

          it('should have a apiUrl defined', inject([MinutesService], (service: MinutesService) => {
            expect(service.apiUrl).toBeDefined();
          }));

          it('getDashboardMinutes should return an array of minutes', async(inject([MinutesService, HttpTestingController],
            (service: MinutesService, backend: HttpTestingController) => {
              service.getDashboardMinutes().subscribe(response => {
                expect(response).toBeTruthy();
                expect(response.length).toBeTruthy();
              });
                backend.expectOne(apiUrl + 'minutes?limit=5').flush([{'1': '1'}, {'2': '2'}], {status: 200, statusText: 'OK'});
          })));

          it('getDashboardMinutes should return an empty array if no minutes', async(inject([MinutesService, HttpTestingController],
            (service: MinutesService, backend: HttpTestingController) => {
              service.getDashboardMinutes().subscribe(response => {
                expect(response).toEqual([]);
              });
                backend.expectOne(apiUrl + 'minutes?limit=5').flush([], {status: 200, statusText: 'OK'});
          })));

          it('getDashboardMinutes should return an empty array if call fails', async(inject([MinutesService, HttpTestingController],
            (service: MinutesService, backend: HttpTestingController) => {
              service.getDashboardMinutes().subscribe(response => {
                expect(response).toEqual([]);
              });
              backend.expectOne(apiUrl + 'minutes?limit=5').flush([], {status: 500, statusText: 'Internal Server Error'});
          })));
        });
        `
        },
          {
            pageType: 'minutes.service.ts',
            description: `Your service call 'getDashboardAgendas()' returns an Observable so you can subscribe to that call.`,
            code: `
        import { Injectable } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { Observable } from 'rxjs/Rx';

        @Injectable()
        export class MinutesService {

          public apiUrl = 'https://api.qa.allaboutminutes.com/api/';

          constructor(private http: HttpClient) { }

          public getDashboardMinutes() {
            return this.http.get(this.apiUrl + 'minutes?limit=5')
            .map((response: any) => {
              return response;
            })
            .catch(() => Observable.of([]));
          }
        }
        `
          }]
      }, {
        title: 'Click a button in a test',
        description: `
      This test will show you how to click a button and check if the function it calls actually gets called.
      <br>
      `,
        codeExample: [
          {
            pageType: 'top-nav.component.html',
            description: `Notice the id fiels for both controls, 'profile' and 'logout',
          we refrence these fields when we search the dom for these controls`,
            code: `
          <mat-toolbar>
            <span>Dashboard</span>
            <span class="example-spacer"></span>
            <button id="profile" mat-button [matMenuTriggerFor]="menu">Profile</button>
            <mat-menu #menu="matMenu">
              <button id="logout" mat-menu-item (click)="logout()">Logout</button>
            </mat-menu>
          </mat-toolbar>
          `
          },
          {
            pageType: 'top-nav.component.specs.ts',
            description: `The main test is the 'logout button functionality' test, here we refrence the controls by their id's.`,
            code: `
        import { async, ComponentFixture, TestBed } from '@angular/core/testing';
        import { HttpClientModule, HttpClient } from '@angular/common/http';
        import { Router, Routes } from '@angular/router';
        import { TopNavComponent } from './top-nav.component';
        import { LoginService } from '../login/login.service';

        import { By } from '@angular/platform-browser';

        describe('TopNavComponent', () => {
          let component: TopNavComponent;
          let fixture: ComponentFixture<TopNavComponent>;
          let spy: any;

          beforeEach(async(() => {
            TestBed.configureTestingModule({
              declarations: [ TopNavComponent ],
              providers: [
                LoginService,
                {
                  provide: Router
                }
              ],
              imports: [
                HttpClientModule
              ]
            })
            .compileComponents();
          }));

          beforeEach(() => {
            fixture = TestBed.createComponent(TopNavComponent);
            component = fixture.componentInstance;
            fixture.detectChanges();
          });

          it('should create', () => {
            expect(component).toBeTruthy();
          });

          it('should have a logout function', () => {
            expect(component.logout).toBeDefined();
          });

          it('logout button functionality', () => {
            spy = spyOn(component, 'logout');
            fixture.debugElement.query(By.css('#profile')).nativeElement.click();
            const button = fixture.debugElement.query(By.css('#logout')).nativeElement;
            expect(button).toBeTruthy();
            button.click();
            expect(spy).toHaveBeenCalled();
          });

        });

        `
          },
          {
            pageType: 'top-nav.component.ts',
            description: `Your service call 'getDashboardAgendas()' returns an Observable so you can subscribe to that call.`,
            code: `
        import { Component, OnInit } from '@angular/core';

        import { LoginService } from '../login/login.service';

        @Component({
          selector: 'app-top-nav',
          templateUrl: './top-nav.component.html',
          styleUrls: ['./top-nav.component.css'],
          providers: [ LoginService ]
        })
        export class TopNavComponent implements OnInit {

          constructor(public service: LoginService) { }

          ngOnInit() {
          }

          public logout() {
            this.service.logout();
          }
        }
        `
          }]
      },
      {
        title: 'Asserting “throw new Error”',
        description: `
      Test how error handeling and assert errors being thrown
      <br>
       'something about the test'
      `,
        codeExample: [
          {
            pageType: 'component.specs.ts',
            description: 'This test just creates the component and then checks the components html for a tag',
            code: ``
          },
          {
            pageType: 'component.ts',
            description: '',
            code: ``
          }
        ]
      }

    ];
    console.log(this.recipes);

    this.selected = this.recipes[0];
  }

  public select(item): void {
    this.selected = item;
    this.loading = true;
    setTimeout(() => {
      this.loading = false
    }, 100)
  }

}


// TODO automate capturing test
//
// {
//   title: 'Asserting “throw new Error”',
//     description: `
//       Test how error handeling and assert errors being thrown
//       <br>
//        'something about the test'
//       `,
//   codeExample: [
//   {
//     pageType: 'component.specs.ts',
//     description: 'This test just creates the component and then checks the components html for a tag',
//     code: ``
//   },
//   {
//     pageType: 'component.ts',
//     description: '',
//     code: ``
//   }
// ]
// }
