import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitTestRecipeComponent } from './unit-test-recipe.component';
import { MatCardModule, MatExpansionModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('UnitTestRecipeComponent', () => {
  let component: UnitTestRecipeComponent;
  let fixture: ComponentFixture<UnitTestRecipeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnitTestRecipeComponent ],
      imports: [
        MatCardModule,
        MatExpansionModule,
        BrowserAnimationsModule
    ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitTestRecipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
