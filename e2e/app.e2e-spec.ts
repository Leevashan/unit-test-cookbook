import { UnitTestRecipesPage } from './app.po';

describe('unit-test-recipes App', () => {
  let page: UnitTestRecipesPage;

  beforeEach(() => {
    page = new UnitTestRecipesPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
